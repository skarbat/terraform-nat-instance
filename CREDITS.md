# CREDITS

This project was originally created by Int128:

 * https://github.com/int128

It's original source code is located here:

 * https://github.com/int128/terraform-aws-nat-instance
